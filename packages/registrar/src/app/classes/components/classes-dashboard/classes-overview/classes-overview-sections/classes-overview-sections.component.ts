import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';

@Component({
  selector: 'app-classes-overview-sections',
  templateUrl: './classes-overview-sections.component.html',
  styleUrls: ['./classes-overview-sections.component.scss']
})
export class ClassesOverviewSectionsComponent implements OnInit {

  constructor(private _activatedRoute: ActivatedRoute, private _context: AngularDataContext) { }

  public class;

  async ngOnInit() {

    this.class = await this._context.model('CourseClasses')
      .where('id').equal(this._activatedRoute.snapshot.params.id)
      .expand('sections($expand=instructors($expand=instructor))')
      .getItem();
  }

}
