import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';

@Component({
  selector: 'app-classes-overview',
  templateUrl: './classes-overview.component.html',
  styleUrls: ['./classes-overview.component.scss']
})
export class ClassesOverviewComponent implements OnInit {

  public class;

  constructor(private _activatedRoute: ActivatedRoute, private _context: AngularDataContext) { }

  async ngOnInit() {
    this.class = await this._context.model('CourseClasses')
      .where('id').equal(this._activatedRoute.snapshot.params.id)
      .expand('course($expand=courseStructureType),status,period')
      .getItem();
  }

}
