import { Component, OnInit, ViewChild, Input, EventEmitter } from '@angular/core';
import { ActivatedTableService } from 'packages/registrar/src/app/tables/tables.activated-table.service';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';

@Component({
  selector: 'app-classes-sections',
  templateUrl: './classes-sections.component.html',
  styleUrls: ['./classes-sections.component.scss']
})
export class ClassesSectionsComponent implements OnInit {


  constructor(private _activatedRoute: ActivatedRoute,
              private _activatedTable: ActivatedTableService,
              private _context: AngularDataContext

  ) { }
  public class;

  async ngOnInit() {

    this.class = await this._context.model('CourseClasses')
      .where('id').equal(this._activatedRoute.snapshot.params.id)
      .expand('sections($expand=instructors($expand=instructor))')
      .getItem();
  }


}
