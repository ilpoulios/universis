import { Component, OnInit, ViewChild, Input, EventEmitter } from '@angular/core';
import { ActivatedTableService } from 'packages/registrar/src/app/tables/tables.activated-table.service';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';

@Component({
  selector: 'app-classes-exams',
  templateUrl: './classes-exams.component.html',
  styleUrls: ['./classes-exams.component.scss']
})
export class ClassesExamsComponent implements OnInit {

  public classExams: any;

  constructor(private _activatedRoute: ActivatedRoute,
              private _activatedTable: ActivatedTableService,
              private _context: AngularDataContext

  ) { }

  async ngOnInit() {

    this.classExams = await this._context.model('CourseExamClasses')
    .where('courseClass').equal(this._activatedRoute.snapshot.params.id)
    .expand('courseExam($expand=examPeriod,status,completedByUser,year,course($expand=department))')
    .orderByDescending('courseExam/year')
    .thenByDescending('courseExam/examPeriod')
    .getItems();
  }


  }
