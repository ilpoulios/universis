import {Component, OnInit} from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {ActivatedRoute} from '@angular/router';
import * as COURSES_LIST_CONFIG from '../courses-table/courses-table.config.list.json';
import {cloneDeep} from 'lodash';
import {TemplatePipe, UserActivityService} from '@universis/common';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-courses-root',
  templateUrl: './courses-root.component.html',
  providers: [TemplatePipe]
})
export class CoursesRootComponent implements OnInit {
  public course: any;
  public tabs: any[];
  public actions: any[];
  public config: any;
  public isCreate = false;
  public allowedActions: any[];
  public edit: any;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext,
              private _userActivityService: UserActivityService,
              private _translateService: TranslateService,
              private _template: TemplatePipe
            ) { }

  async ngOnInit() {
    this.tabs = this._activatedRoute.routeConfig.children.filter( route => typeof route.redirectTo === 'undefined' );

    if (this._activatedRoute.snapshot.url.length > 0 &&
        this._activatedRoute.snapshot.url[0].path === 'create') {
      this.isCreate = true;
    } else {
      this.isCreate = false;
    }

    if (this._activatedRoute.snapshot.params.id) {
      this.course = await this._context.model('Courses')
          .where('id').equal(this._activatedRoute.snapshot.params.id)
          .select('id', 'name', 'displayCode')
          .getItem();
    }

    if (this.course) {
      this._userActivityService.setItem({
        category: this._translateService.instant('Courses.Title'),
        description: this._translateService.instant(this.course.displayCode + ' - ' + this.course.name),
        url: window.location.hash.substring(1), // get the path after the hash
        dateCreated: new Date
      });

      // @ts-ignore
      this.config = cloneDeep(COURSES_LIST_CONFIG as TableConfiguration);

      if (this.config.columns && this.course) {
        // get actions from config file
        this.actions = this.config.columns.filter(x => {
          return x.actions;
        })
          // map actions
          .map(x => x.actions)
          // get list items
          .reduce((a, b) => b, 0);

        // filter actions with student permissions
        this.allowedActions = this.actions.filter(x => {
          if (x.role) {
            if (x.role === 'action') {
              return x;
            }
          }
        });

        this.edit = this.actions.find(x => {
          if (x.role === 'edit') {
            x.href = this._template.transform(x.href,this.course);
            return x;
          }
        });

        this.actions = this.allowedActions;
        this.actions.forEach(action => {
          action.href = this._template.transform(action.href, this.course);
        });

      }
    }
  }
}
