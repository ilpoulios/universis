import {AfterViewInit, Component, ElementRef, OnInit} from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {ActivatedRoute} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {LoadingService} from '@universis/common';

@Component({
  selector: 'app-students-internships',
  templateUrl: './students-internships.component.html',
  styleUrls: ['./students-internships.component.scss']
})
export class StudentsInternshipsComponent implements OnInit {

  public model: any;

  constructor(private _element: ElementRef,
              private _activatedRoute: ActivatedRoute,
              private _translate: TranslateService,
              private _loadingService: LoadingService,
              private _context: AngularDataContext) {
  }

  async ngOnInit() {
    this._loadingService.showLoading();
    this.model = await this._context.model('Internships')
      .where('student').equal(this._activatedRoute.snapshot.params.id)
      .expand('status,internshipPeriod,department,student($expand=person)')
      .getItems();
    this._loadingService.hideLoading();
  }

}
