import {Component, Input, OnInit} from '@angular/core';
import {AngularDataContext} from '@themost/angular';

@Component({
  selector: 'app-students-overview-scholarships',
  templateUrl: './students-overview-scholarships.component.html',
  styleUrls: ['./students-overview-scholarships.component.scss']
})
export class StudentsOverviewScholarshipsComponent implements OnInit {

  public scholarships: any;
  @Input() studentId: number;

  constructor(private _context: AngularDataContext) { }

  async ngOnInit() {
    this.scholarships = await this._context.model('StudentScholarships')
      .asQueryable()
      .where('student').equal(this.studentId)
      .getItems();
  }

}
