import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AngularDataContext} from '@themost/angular';

@Component({
  selector: 'app-students-overview',
  templateUrl: './students-overview.component.html',
  styleUrls: ['./students-overview.component.scss']
})
export class StudentsOverviewComponent implements OnInit {

  public studentId;
  public showMore = true;
  public student;

  constructor(private _activatedRoute: ActivatedRoute, private _context: AngularDataContext) { }

  async ngOnInit() {
    this.studentId = this._activatedRoute.snapshot.params.id;
    this.student = await this._context.model('Students')
      .where('id').equal(this.studentId)
      .expand('person($expand=gender), department, studyProgram')
      .getItem();
  }

}
