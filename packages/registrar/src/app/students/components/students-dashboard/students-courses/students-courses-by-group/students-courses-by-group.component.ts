import { Component, OnInit, ViewChild } from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {ActivatedRoute} from '@angular/router';
import { AdvancedSearchFormComponent } from 'packages/registrar/src/app/tables/components/advanced-search-form/advanced-search-form.component';
import * as SEARCH_CONFIG from './student-courses-table.search.list.json';
import { Pipe, PipeTransform } from '@angular/core';
import {TemplatePipe} from '@universis/common';

@Component({
  selector: 'app-students-courses-by-group',
  templateUrl: './students-courses-by-group.component.html',
  styleUrls: ['./students-courses-by-group.component.scss']
})
export class StudentsCoursesByGroupComponent implements OnInit {
  public model: any;
  public data: any;
  public groups = [];

  @ViewChild('search') search: AdvancedSearchFormComponent;
  collapsed = true;

  public groupTypes = [
    {
      key: 'semester',
      prop: 'semester.alternateName',
      name: 'Students.Semester'
    },
    {
      key: 'type',
      prop: 'courseType.id',
      name: 'Students.CourseType'
    }
  ];
  public selectedGroupType = this.groupTypes[0];
  private searchExpression = '(student eq ${student} and (indexof(course/name, \'${text}\') ge 0 or indexof(course/displayCode, \'${text}\') ge 0 or indexof(course/courseArea/name, \'${text}\') ge 0 or indexof(course/courseCategory/name, \'${text}\') ge 0 or indexof(courseType/name, \'${text}\') ge 0))';
  private allStudentCourses: any;

  public defaultGradeScale: any;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext,
              private _template: TemplatePipe) {
  }

  async  ngOnInit() {
    this.defaultGradeScale = await this._context.model('Students')
    .where('id').equal(this._activatedRoute.snapshot.params.id)
    .expand('studyProgram').getItem().then( res => res.studyProgram.gradeScale);

    this.model = await this._context.model('StudentCourses')
    .where('student').equal(this._activatedRoute.snapshot.params.id)
    .expand('course($expand=instructor,gradeScale),gradeExam($expand=instructors($expand=instructor))')
    .take(-1)
    .getItems();

    this.allStudentCourses = this.model;

    this.selectedGroupType = this.groupTypes[0];
    this.search.form = SEARCH_CONFIG;
    this.search.ngOnInit();

    this.parseCourseParts();
  }

  parseCourseParts() {
    // get parent courses for orphaned children after search
    const parents = this.allStudentCourses.filter(el => {
        return !this.model.map(x => x.course.id).includes(el.course.id);
    }).filter(el => {
        return this.model.map(x => x.parentCourse).includes(el.course.id);
    });

    // remove course parts and add parents
    this.data = this.model.filter(studentCourse => {
      return studentCourse.courseStructureType && studentCourse.courseStructureType.id !== 8;
    }).concat(parents);

    // add courseParts as courses to each parent course
    this.data.forEach( studentCourse => {
      // get course parts
      if (studentCourse.courseStructureType && studentCourse.courseStructureType.id === 4) {
        studentCourse.courses = this.model.filter(course => {
          return course.parentCourse === studentCourse.course.id;
        }).sort((a, b) => {
          return a.course.displayCode < b.course.displayCode ? -1 : 1;
        });
      }
    });
  }

  async onSearchKeyDown(event: any) {
    const q = this._context.model('StudentCourses').asQueryable();

    const searchText = (<HTMLInputElement>event.target);
    if (searchText && event.keyCode === 13) {
      if (searchText && searchText.value && this.searchExpression) {
        // validate search text in double quotes
        if (/^"(.*?)"$/.test(searchText.value)) {
          q.setParam('$filter',
            this._template.transform(this.searchExpression, {
              student: this._activatedRoute.snapshot.params.id,
              text: searchText.value.replace(/^"|"$/g, '')
            }));
        } else {
          // try to split words
          const words = searchText.value.split(' ');
          // join words to a single filter
          const filter = words.map( word => {
            return this._template.transform(this.searchExpression, {
              student: this._activatedRoute.snapshot.params.id,
              text: word
            });
          }).join (' and ');
          // set filter
          q.setParam('$filter', filter);
        }
      } else {
        // use only student filter
        q.setParam('$filter', 'student eq ' + this._activatedRoute.snapshot.params.id);
      }
      this.model = await q.expand('course($expand=instructor,gradeScale),gradeExam($expand=instructors($expand=instructor))')
      .take(-1).getItems();

      this.parseCourseParts();
    }
  }

  async advancedSearch(event?: any) {
    if (Array.isArray(this.search.form.criteria)) {
      const expressions = [];
      const filter = this.search.filter;
      const values = this.search.formComponent.formio.data;

      this.search.form.criteria.forEach((x) => {
        if (Object.prototype.hasOwnProperty.call(filter, x.name)) {
          // const nameFilter = this.convertToString(filter[x.name]);
          if (filter[x.name] !== 'undefined') {
            expressions.push(this._template.transform(x.filter, Object.assign({
              value: filter[x.name]
            }, values)));
          }
        }
      });
      expressions.push('(student eq ' + this._activatedRoute.snapshot.params.id + ')');

      // create client query
      const q = this._context.model('StudentCourses').asQueryable();
      q.setParam('filter', expressions.join(' and '));

      this.model = await q.expand('course($expand=instructor,gradeScale),gradeExam($expand=instructors($expand=instructor))')
      .take(-1).getItems();

      this.parseCourseParts();
    }
  }


}

@Pipe({
    name: 'gradeAverage'
})
export class GradeAveragePipe implements PipeTransform {
  transform(items: any[]): any {
    const courses = items.filter(studentCourse => {
      return studentCourse.courseStructureType.id !== 8 && studentCourse.isPassed && studentCourse.calculateUnits
             && studentCourse.course.gradeScale.scaleType !== 3 && studentCourse.grade !== null;
    });
    if (courses && courses.length > 0) {
      return courses.reduce((a, b) => a + b.grade, 0) / courses.length;
    } else {
      return 0;
    }
  }
}
