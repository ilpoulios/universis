import {Component, OnDestroy, OnInit} from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {ActivatedRoute} from '@angular/router';
import {Subscription} from 'rxjs';
import {ErrorService} from '@universis/common';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-study-programs-preview',
  templateUrl: './study-programs-preview.component.html'
})
export class StudyProgramsPreviewComponent implements OnInit, OnDestroy {
  private paramSubscription: Subscription;
  public studySpecializations: any[];

  constructor(private _context: AngularDataContext,
              private _activatedRoute: ActivatedRoute,
              private _errorService: ErrorService,
              public _translateService: TranslateService) {
    //
  }

  ngOnInit() {
    this.paramSubscription = this._activatedRoute.params.subscribe((params) => {
      this._context.model('StudyProgramSpecialties')
          .where('studyProgram').equal(params.id)
          .orderBy('specialty')
          .getItems().then((items) => {
        this.studySpecializations = items;
      }).catch((err) => {
        this._errorService.showError(err);
      });
    });
  }

  ngOnDestroy(): void {
    if (this.paramSubscription) {
      this.paramSubscription.unsubscribe();
    }
  }

}
