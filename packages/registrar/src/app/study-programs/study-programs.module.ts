import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import { StudyProgramsHomeComponent } from './components/study-programs-home/study-programs-home.component';
import {StudyProgramsRoutingModule} from './study-programs.routing';
import {StudyProgramsSharedModule} from './study-programs.shared';
import { StudyProgramsTableComponent } from './components/study-programs-table/study-programs-table.component';
import {TranslateModule} from '@ngx-translate/core';
import {TablesModule} from '../tables/tables.module';
import { StudyProgramsRootComponent } from './components/study-programs-root/study-programs-root.component';
import { StudyProgramsPreviewComponent } from './components/preview/study-programs-preview.component';
import { StudyProgramsPreviewGeneralComponent } from './components/preview/preview-general/study-programs-preview-general.component';
import {FormsModule} from '@angular/forms';
import {SharedModule} from '@universis/common';
import { StudyProgramsPreviewCoursesComponent } from './components/preview/preview-courses/study-programs-preview-courses.component';
import { CoursesSharedModule } from '../courses/courses.shared';
import {ElementsModule} from '../elements/elements.module';
import { ProgramCoursePreviewComponent } from './components/program-course-preview/program-course-preview.component';
import { ProgramCoursePreviewGeneralComponent } from './components/program-course-preview/program-course-preview-general/program-course-preview-general.component';
import { MostModule } from '@themost/angular';
import { StudyProgramsAdvancedTableSearchComponent } from './components/study-programs-table/study-programs-advanced-table-search.component';
import { StudyProgramsCoursesAdvancedTableSearchComponent } from './components/preview/preview-courses/study-programs-courses-advanced-table-search.component';
import {RouterModalModule} from '@universis/common/routing';
import {RegistrarSharedModule} from '../registrar-shared/registrar-shared.module';
import {AdvancedFormsModule} from '@universis/forms';
import { StudyProgramsPreviewGeneralProfileComponent } from './components/preview/preview-general/study-programs-preview-general-profile/study-programs-preview-general-profile.component';
import { StudyProgramsPreviewGeneralSpecialtiesComponent } from './components/preview/preview-general/study-programs-preview-general-specialties/study-programs-preview-general-specialties.component';
import { StudyProgramsPreviewGeneralStatisticsComponent } from './components/preview/preview-general/study-programs-preview-general-statistics/study-programs-preview-general-statistics.component';
import {ChartsModule} from 'ng2-charts';
import { StudyProgramsPreviewGeneralStatisticsStudentStatusComponent } from './components/preview/preview-general/study-programs-preview-general-statistics-student-status/study-programs-preview-general-statistics-student-status.component';
import {TooltipModule} from 'ngx-bootstrap';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    TablesModule,
    StudyProgramsSharedModule,
    StudyProgramsRoutingModule,
    SharedModule,
    FormsModule,
    CoursesSharedModule,
    ElementsModule,
    MostModule,
    RouterModalModule,
    RegistrarSharedModule,
    AdvancedFormsModule,
    ChartsModule,
    TooltipModule.forRoot()
  ],
  declarations: [StudyProgramsHomeComponent, StudyProgramsTableComponent,
    StudyProgramsRootComponent, StudyProgramsPreviewComponent, StudyProgramsPreviewGeneralComponent,
    StudyProgramsPreviewCoursesComponent, ProgramCoursePreviewComponent,
    ProgramCoursePreviewGeneralComponent, StudyProgramsAdvancedTableSearchComponent,
    StudyProgramsCoursesAdvancedTableSearchComponent,
    StudyProgramsPreviewGeneralProfileComponent,
    StudyProgramsPreviewGeneralSpecialtiesComponent,
    StudyProgramsPreviewGeneralStatisticsComponent,
    StudyProgramsPreviewGeneralStatisticsStudentStatusComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class StudyProgramsModule { }
