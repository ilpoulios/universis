import { HttpClient, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { AngularDataContext } from '@themost/angular';
import { Subscription, BehaviorSubject, Observable } from 'rxjs';
import 'rxjs/add/observable/of';
import { Injectable } from '@angular/core';
import {ConfigurationService, ErrorService} from '@universis/common';
import {ResponseError, TextUtils} from '@themost/client';

import cloneDeep = require('lodash/cloneDeep');
import * as SIGN_PARAMETERS from './sign-parameters.json';
import {ServiceUrlPreProcessor} from '@universis/forms';
import {TranslateService} from '@ngx-translate/core';
import 'rxjs/add/operator/map';

export interface SignerServiceConfiguration {
    timestampServer?: string;
    defaultLocation?: string;
}

interface SettingsConfiguration {
    signer?: SignerServiceConfiguration;
}

@Injectable()
export class SignerService {

    static SINGER_URI = 'http://localhost:2465/';
    static DEFAULT_SIGN_POSITION = '20,10,320,120';
    private _status: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    private _signerBasicAuthorization: string;
  constructor(private _context: AngularDataContext,
              private _http: HttpClient,
              private _errorService: ErrorService,
              private _translateService: TranslateService,
              private _configurationService: ConfigurationService) {
    //
  }

  public get status(): BehaviorSubject<any> {
    setTimeout(() => {
        // call signer service and return value
    return this._http.get(SignerService.SINGER_URI).subscribe((response: HttpResponse<any>) => {
            this._status.next({
                ok: response.ok,
                status: response.status,
                statusText: response.statusText
            });
        }, (err: HttpErrorResponse) => {
            this._status.next({
                ok: err.ok,
                status: err.status,
                statusText: err.statusText
            });
        });
    }, 500);
    return this._status;
  }

  public authenticate({ username, password, rememberMe }: { username: string; password: string; rememberMe?: boolean; }): Promise<void> {
    const basicAuthorization = 'Basic ' + btoa(`${username}:${password}`);
    return this._http.get(new URL('/keystore/certs', SignerService.SINGER_URI).toString(), {
        headers: {
            'Authorization': basicAuthorization
        }
    }).toPromise().then(() => {
      if (rememberMe) {
        // set item to session storage
        sessionStorage.setItem('signer.auth', basicAuthorization);
      } else {
        // otherwise use memory
        this._signerBasicAuthorization = basicAuthorization;
      }
    });
  }
  public get authorization() {
    return sessionStorage.getItem('signer.auth') || this._signerBasicAuthorization;
  }
  /**
   * Gets a list of the available certificates
   */
  public getCertificates(): Promise<any> {

    if (this.authorization == null) {
      return Promise.reject(new ResponseError('Unauthorized', 401.5));
    }
    return this._http.get(new URL('/keystore/certs', SignerService.SINGER_URI).toString(), {
        headers: {
            'Accept': 'application/json',
            'Authorization': this.authorization
        },
        responseType: 'json'
    }).toPromise();
  }

    /**
     * Queries signer application status
     */
  public queryStatus(): Observable<any> {
      return new Observable( subscriber => {
          return this._http.get(SignerService.SINGER_URI).subscribe((response: HttpResponse<any>) => {
              subscriber.next({
                  ok: response.ok,
                  status: response.status,
                  statusText: response.statusText
              });
          }, (err: HttpErrorResponse) => {
              subscriber.next({
                  ok: err.ok,
                  status: err.status,
                  statusText: err.statusText
              });
          });
      });
  }

  public signDocument(blob: any, thumbprint: string, position?: string): Promise<any> {
    // validate authentication
    if (this.authorization == null) {
      return Promise.reject(new ResponseError('Unauthorized', 401));
    }
    const formData = new FormData();
    // set position or default
    formData.append('position', position || SignerService.DEFAULT_SIGN_POSITION);
    // set certificate thumbprint
    formData.append('thumbprint', thumbprint);
    // set file
    formData.append('file', blob, 'unsigned.pdf');
    // get signer configuration
    const signerSettings = (<SettingsConfiguration>this._configurationService.settings).signer;
    // get timestamp server
    let timestampServer: string;
    if (signerSettings != null) {
        timestampServer = signerSettings.timestampServer;
    }
    // if timestamp server is defined
    if (timestampServer != null && timestampServer.length > 0) {
        formData.append('timestampServer', timestampServer);
    }
    // sign document
    return this._http.post(new URL('/sign', SignerService.SINGER_URI).toString(), formData, {
      headers: {
        'Authorization': this.authorization
      },
      responseType: 'blob',
      observe: 'response'
    }).toPromise().then( (response) => {
      return response.body;
    });
  }

  public async replaceDocument(document: any, blob: Blob) {
    let item: any;
    if (Object.prototype.hasOwnProperty.call(document, 'id')) {
      item = await this._context.model('DocumentNumberSeriesItems').where('id').equal(document.id).getItem();
    } else if (Object.prototype.hasOwnProperty.call(document, 'url')) {
      item = await this._context.model('DocumentNumberSeriesItems').where('url').equal(document.url).getItem();
    }
    if (item == null) {
      throw new ResponseError('The specified document cannot be found', 404.4);
    }
    const replaceURL = this._context.getService().resolve(`DocumentNumberSeriesItems/${item.id}/replace`);
    const formData = new FormData();
    // set name
    formData.append('name', item.name);
    // set certificate thumbprint
    formData.append('contentType', item.contentType);
    formData.append('published', document.published);
    if (document.published) {
        formData.append('datePublished', TextUtils.escape(document.datePublished || new Date()).replace(/'/g, ''));
    }
    if (Object.prototype.hasOwnProperty.call(document, 'signed')) {
        formData.append('signed', document.signed);
    }
    // set file
    formData.append('file', blob, item.name);
    // sign document
    return this._http.post(replaceURL, formData, {
      headers: this._context.getService().getHeaders(),
      responseType: 'json',
      observe: 'response'
    }).toPromise().then( (response) => {
      return response.body;
    });
  }

    async getSignFormFor(document: any) {
        const form  = <any>cloneDeep(SIGN_PARAMETERS);
        new ServiceUrlPreProcessor(this._context).parse(form);
        form.params = form.params || {};
        Object.assign(form.params, document);
        return form;
    }

    setLastCertificate(thumbprint: string) {
        return sessionStorage.setItem('signer.lastCertificate', thumbprint);
    }

    getLastCertificate(): string {
        return sessionStorage.getItem('signer.lastCertificate');
    }

    destroy() {
      if (this._signerBasicAuthorization) {
          this._signerBasicAuthorization = null;
      }
    }


}
