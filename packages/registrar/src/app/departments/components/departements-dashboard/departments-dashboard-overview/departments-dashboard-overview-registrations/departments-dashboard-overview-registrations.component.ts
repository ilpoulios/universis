import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AngularDataContext} from '@themost/angular';
import {TranslateService} from '@ngx-translate/core';
import {ActiveDepartmentService} from "../../../../../registrar-shared/services/activeDepartmentService.service";

@Component({
  selector: 'app-departments-dashboard-overview-registrations',
  templateUrl: './departments-dashboard-overview-registrations.component.html',
})
export class DepartmentsDashboardOverviewRegistrationsComponent  implements OnInit {
  public activeDepartment: any;
  public registrationStatus: string;
  public departmentsId: any;
  public department: any;
  constructor(private _activatedRoute: ActivatedRoute,
              private _translate: TranslateService,
              private _context: AngularDataContext,
              private _activeDepartmentService: ActiveDepartmentService) {}

  async ngOnInit() {
    this.activeDepartment = await this._activeDepartmentService.getActiveDepartment();
    this.registrationStatus = this.getRegistrationStatus();
    this.departmentsId = this._activatedRoute.snapshot.params.id;

    this.department = await this._context.model('LocalDepartments')
      .where('id').equal(this._activatedRoute.snapshot.params.id)
      .getItem();
  }
  getRegistrationStatus(): string {
    const startingDate = new Date(this.activeDepartment.registrationPeriodStart);
    const endingDate = new Date(this.activeDepartment.registrationPeriodEnd);
    const now = new Date();

    if (startingDate > now) {
      return 'pendingStart';
    } else if ( this.activeDepartment.isRegistrationPeriod && (startingDate <= now && endingDate >= now)) {
      return 'open';
    } else {
      return 'close';
    }
  }
}
