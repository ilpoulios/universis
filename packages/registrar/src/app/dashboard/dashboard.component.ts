import { Component,  OnInit } from '@angular/core';
import {ActiveDepartmentService} from '../registrar-shared/services/activeDepartmentService.service';

@Component({
  selector: 'app-theses-home',
  templateUrl: 'dashboard.component.html'
})
export class DashboardComponent implements OnInit {

  constructor(private _activeDepartmentService: ActiveDepartmentService) { }

  public activeDepartment: any;
  public registrationStatus: string;

  async ngOnInit() {
    this.activeDepartment = await this._activeDepartmentService.getActiveDepartment();
    this.registrationStatus = this.getRegistrationStatus();
  }


  /**
   *
   * Given the active department returns the registration status.
   *
   */
  getRegistrationStatus(): string {

    if (!this.activeDepartment) {
      return 'unknown';
    }

    const startingDate = new Date(this.activeDepartment.registrationPeriodStart);
    const endingDate = new Date(this.activeDepartment.registrationPeriodEnd);
    const now = new Date();

    if (startingDate > now) {
      return 'pendingStart';
    } else if ( this.activeDepartment.isRegistrationPeriod && (startingDate <= now && endingDate >= now)) {
      return 'open';
    } else {
      return 'close';
    }
  }
}
