import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';

@Component({
  selector: 'app-course-exam-upload-actions',
  templateUrl: './course-exam-upload-actions.component.html',
  styleUrls: ['./course-exam-upload-actions.component.scss']
})
export class CourseExamUploadActionsComponent implements OnInit {

  public uploadGrades: any;
  public active: number;
  public examId: any;

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _context: AngularDataContext
  ) { }

  async ngOnInit() {

    this.examId = this._activatedRoute.snapshot.params.id;

    this.uploadGrades = await this._context.model('CourseExams/' + this._activatedRoute.snapshot.params.id + '/actions')
      .asQueryable()
      .expand('owner,additionalResult,grades($select=action,count(id) as totalCount;$groupby=action)')
      .where('additionalResult').notEqual(null)
      .orderByDescending('dateCreated')
      .take(-1)
      .getItems();

    this.active = (this.uploadGrades).filter(x => {
      return x.actionStatus && x.actionStatus.alternateName === 'ActiveActionStatus';
    }).length;
  }

}
