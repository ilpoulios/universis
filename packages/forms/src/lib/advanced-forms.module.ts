import {ModuleWithProviders, NgModule, Optional, SkipSelf} from '@angular/core';
import {MostModule} from '@themost/angular';
import {AdvancedFormResolver, AdvancedFormsService} from './advanced-forms.service';
import {HttpClientModule} from '@angular/common/http';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {environment} from '../environments/environment';
import {FORMS_LOCALES} from './i18n';
import {FormioAppConfig, FormioModule} from 'angular-formio';
import { AdvancedFormComponent } from './advanced-form.component';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {AdvancedFormModalComponent} from './advanced-form-modal.component';
import {AppConfig} from '../formio-config';



@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    MostModule,
    TranslateModule,
    FormioModule,
    RouterModule
  ],
  declarations: [
    AdvancedFormComponent,
    AdvancedFormModalComponent
  ],
  exports: [
      AdvancedFormComponent,
      AdvancedFormModalComponent
  ]
})
export class AdvancedFormsModule {

  constructor( @Optional() @SkipSelf() parentModule: AdvancedFormsModule, private _translateService: TranslateService) {
    environment.languages.forEach( language => {
      if (FORMS_LOCALES.hasOwnProperty(language)) {
        this._translateService.setTranslation(language, FORMS_LOCALES[language], true);
      }
    });
  }

  static forRoot(): ModuleWithProviders {
    return {
      ngModule: AdvancedFormsModule,
      providers: [
        AdvancedFormsService,
        AdvancedFormResolver,
        {provide: FormioAppConfig, useValue: AppConfig}
      ]
    };
  }
}
