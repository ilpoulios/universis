import {AngularDataContext} from '@themost/angular';
import * as _ from 'lodash';
import {Injectable} from '@angular/core';

@Injectable()
export class LocalUserStorageService {

  constructor(private context: AngularDataContext) {
  }

  /**
   *
   * Indicates whether the localStorage is available
   *
   */
  hasUserStorage(): boolean {
    return !!localStorage;
  }

  async getItem(key: string): Promise<any> {
    const localStorageItem = localStorage.getItem('userLocalStorage');
    let userLocalStorage = {};
    if (localStorageItem) {
      userLocalStorage = JSON.parse(localStorageItem);
    }
    const finalKey = key.replace(/\//g, '.');
    return {
      key: key,
      value: await _.get(userLocalStorage, finalKey, null)
    };
  }

  async setItem(key: string, value: any): Promise<any> {
    const localStorageItem = await localStorage.getItem('userLocalStorage');
    let userLocalStorage = {};
    if (localStorageItem) {
      userLocalStorage = JSON.parse(localStorageItem);
    }
    const finalKey = key.replace(/\//g, '.');
    if (_.has(userLocalStorage, finalKey) && (value === (await this.getItem(finalKey)).value)) {
      return;
    }
    _.set(userLocalStorage, finalKey, value);
    await localStorage.setItem('userLocalStorage', JSON.stringify(userLocalStorage));
  }

  async removeItem(key: string): Promise<any> {
    const localStorageItem = localStorage.getItem('userLocalStorage');
    let userLocalStorage = {};
    if (localStorageItem) {
      userLocalStorage = JSON.parse(localStorageItem);
    }
    const finalKey = key.replace(/\//g, '.');
    if (_.has(userLocalStorage, finalKey)) {
      const result = _.unset(userLocalStorage, finalKey);
      if (result) {
        localStorage.setItem('userLocalStorage', JSON.stringify(userLocalStorage));
        return Promise.resolve();
      } else {
        return Promise.reject('Key could not be removed');
      }
    } else {
      return Promise.reject('Key was not found in the object');
    }
  }
}

@Injectable()
export class SessionUserStorageService {

  constructor(private context: AngularDataContext) {
  }

  async getItem(key: string): Promise<any> {
    const sessionStorageItem = sessionStorage.getItem('userSessionStorage');
    let userSessionStorage = {};
    if (sessionStorageItem) {
      userSessionStorage = JSON.parse(sessionStorageItem);
    }
    const finalKey = key.replace(/\//g, '.');
    return {
      key: key,
      value: await _.get(userSessionStorage, finalKey, null)
    };
  }

  async setItem(key: string, value: any): Promise<any> {
    const sessionStorageItem = sessionStorage.getItem('userSessionStorage');
    let userSessionStorage = {};
    if (sessionStorageItem) {
      userSessionStorage = JSON.parse(sessionStorageItem);
    }
    const finalKey = key.replace(/\//g, '.');
    if (_.has(userSessionStorage, finalKey) && (value === (await this.getItem(finalKey)).value)) {
      return;
    }
    _.set(userSessionStorage, finalKey, value);
    await sessionStorage.setItem('userSessionStorage', JSON.stringify(userSessionStorage));
  }

  async removeItem(key: string): Promise<any> {
    const sessionStorageItem = sessionStorage.getItem('userSessionStorage');
    let userSessionStorage = {};
    if (sessionStorageItem) {
      userSessionStorage = JSON.parse(sessionStorageItem);
    }
    const finalKey = key.replace(/\//g, '.');
    if (_.has(userSessionStorage, finalKey)) {
      const result = _.unset(userSessionStorage, finalKey);
      if (result) {
        sessionStorage.setItem('userSessionStorage', JSON.stringify(userSessionStorage));
        return Promise.resolve();
      } else {
        return Promise.reject('Key could not be removed');
      }
    } else {
      return Promise.reject('Key was not found in the object');
    }
  }
}
